---
title: the.uncultured.observer
email: the.uncultured.observer@protonmail.com
gpg: A23100CCA47B9FA9E1141C6B5C9DC0F61F220BE3
---

**rss**: <https://the.uncultured.observer/rss.xml>

**src**: <https://gitlab.com/the.uncultured.observer/blog>

**medium**: <https://the-uncultured-observer.medium.com>

---

**email**: <the.uncultured.observer@protonmail.com>

**gpg**: [A231 00CC A47B 9FA9 E114  1C6B 5C9D C0F6 1F22 0BE3](https://gitlab.com/the.uncultured.observer/blog/-/snippets/2041835)

---

**Built with**: <https://gitlab.com/the.uncultured.observer/site-gen>
